///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file int.h
/// @version 1.0
///
/// Print the characteristics of the "int", "unsigned int", "long" and "unsigned long" datatypes.
///
/// @author @todo Christopher Aguilar <ciaguila@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   @todo 19/1/2021
///////////////////////////////////////////////////////////////////////////////

extern void doInt();            /// Print the characteristics of the "int" datatype
extern void flowInt();          /// Print the overflow/underflow characteristics of the "int" datatype

extern void doUnsignedInt();      /// Print the characteristics of the "unsigned int" datatype
extern void flowUnsignedInt();    /// Print the overflow/underflow characteristics of the "unsigned int" datatype

extern void doLongInt();    /// Print the characteristics of the "long" datatype
extern void flowLongInt();  /// Print the overflow/underflow characteristics of the "long" datatype

extern void doUnsignedLongInt();    /// Print the characteristics of the "unsigned long" datatype
extern void flowUnsignedLongInt();  /// Print the overflow/underflow characteristics of the "unsigned long" datatype


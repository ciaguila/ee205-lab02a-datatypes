///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file int.c
/// @version 1.0
///
/// Print the characteristics of the "int", "unsigned int", "long"  and "unsigned long" datatypes.
///
/// @author @todo Christopher Aguilar  <ciaguila@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   19/1/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "int.h"


///////////////////////////////////////////////////////////////////////////////
/// int

/// Print the characteristics of the "int" datatype
void doInt() {
   printf(TABLE_FORMAT_INT, "int", sizeof(int)*8, sizeof(int), INT_MIN, INT_MAX);

}


/// Print the overflow/underflow characteristics of the "int" datatype
void flowInt() { int overflow = INT_MAX;
   printf("int overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   int underflow = INT_MIN;
   printf("int underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);

}


///////////////////////////////////////////////////////////////////////////////
/// unsigned int

/// Print the characteristics of the "unsigned int" datatype
void doUnsignedInt() { printf(TABLE_FORMAT_UINT, "unsigned int", sizeof(unsigned int)*8, sizeof(unsigned int), 0, UINT_MAX);

}

/// Print the overflow/underflow characteristics of the "unsigned int" datatype
void flowUnsignedInt() {
   unsigned int overflow = UINT_MAX;
   printf("unsigned int overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   unsigned int underflow = 0;
   printf("unsigned int underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);

}

/// Print the characteristics of the "long int" datatype
void doLongInt() {
   printf(TABLE_FORMAT_LONG, "long", sizeof(long)*8, sizeof(long), LONG_MIN, LONG_MAX);

}


/// Print the overflow/underflow characteristics of the "long int" datatype
void flowLongInt() { long overflow = LONG_MAX;
   printf("long overflow:  %ld + 1 ", overflow++);
   printf("becomes %ld\n", overflow);

   long underflow = LONG_MIN;
   printf("long underflow:  %ld - 1 ", underflow--);
   printf("becomes %ld\n", underflow);

}


///////////////////////////////////////////////////////////////////////////////
/// unsigned long

/// Print the characteristics of the "unsigned long" datatype
void doUnsignedLongInt() { printf(TABLE_FORMAT_ULONG, "unsigned long", sizeof(unsigned long)*8, sizeof(unsigned long), 0, ULONG_MAX);

}

/// Print the overflow/underflow characteristics of the "unsigned long" datatype
void flowUnsignedLongInt() {
   unsigned long int overflow = ULONG_MAX;
   printf("unsigned long overflow:  %llu + 1 ", overflow++);
   printf("becomes %llu\n", overflow);

   unsigned long int underflow = 0;
   printf("unsigned long underflow:  %llu - 1 ", underflow--);
   printf("becomes %llu\n", underflow);

}

